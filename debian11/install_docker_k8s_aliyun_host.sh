#!/bin/bash
# 远程使用方式
# curl -sSL "https://gitee.com/nice9/k8s/raw/master/debian11/install_docker_k8s_aliyun_host.sh" | bash

############################################# Docker ###################################################################
echo "############################################# Docker #############################################################"
# step 1: 安装必要的一些系统工具
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

# step 2: 安装GPG证书, 阿里云服务器使用mirrors.cloud.aliyuncs.com域名的
curl -fsSL http://mirrors.cloud.aliyuncs.com/docker-ce/linux/debian/gpg | sudo apt-key add -
# 使用阿里云域名的
# curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/debian/gpg | sudo apt-key add -

# Step 3: 写入软件源信息, 使用mirrors.cloud.aliyuncs.com域名的
sudo add-apt-repository "deb [arch=amd64] http://mirrors.cloud.aliyuncs.com/docker-ce/linux/debian $(lsb_release -cs) stable"
# 使用阿里云域名的
# sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/debian $(lsb_release -cs) stable"


# Step 4: 更新并安装Docker-CE
sudo apt-get -y update
sudo apt-get -y install docker-ce=5:20.10.24~3-0~debian-bullseye
systemctl enable docker
sudo systemctl is-enabled docker

############################################# Kubernetes ###############################################################
echo "############################################# Kubernetes #########################################################"
##### 增加k8s软件源, 需要注意1.28及以后得版本, k8s软件源的安装方式变了, 参考国内源 阿里云的讲述

# step 1: 安装必要的一些系统工具
apt-get update && apt-get install -y apt-transport-https

# step 2: 安装GPG证书
curl http://mirrors.cloud.aliyuncs.com/kubernetes/apt/doc/apt-key.gpg | apt-key add -

# Step 3: 写入软件源信息
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://mirrors.cloud.aliyuncs.com/kubernetes/apt/ kubernetes-xenial main
EOF

# Step 4: 更新并安装kubelet kubeadm kubectl
apt-get update
apt-get install -y kubelet=1.23.17-00 kubeadm=1.23.17-00 kubectl=1.23.17-00

# Setp 5: kubelet开机自启动与自启动验证
sudo systemctl enable kubelet
sudo systemctl is-enabled kubelet

###### 安装指定版本的kubelet:
# Step 1: 查找kubelet的版本:
# apt-cache madison kubelet

# Step 2: 安装指定版本的kubelet:
# sudo apt-get -y install kubelet=[VERSION]

############################################# Bash Completion ##########################################################
echo "############################################# Bash Completion ###################################################"
# 安装bash补全
apt install -y bash-completion

# 执行bash_completion
source /usr/share/bash-completion/bash_completion

# 加载 completion
source <(kubectl completion bash)

# 写入系统配置中
cat << EOF >> ~/.bashrc
source /usr/share/bash-completion/bash_completion
source <(kubectl completion bash)
EOF

