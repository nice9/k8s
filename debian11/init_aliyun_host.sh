#!/bin/bash
# 远程使用方式
# curl -sSL "https://gitee.com/nice9/k8s/raw/master/debian11/init_aliyun_host.sh" | bash

############################################# 更换源 ####################################################################
echo "############################################# 更换源 ##############################################################"
BAK_FILE_NAME=/etc/apt/sources.list.bak.$(date +'%Y%m%d%H%M%S')
echo "备份原始源配置至"$BAK_FILE_NAME
sudo mv /etc/apt/sources.list ${BAK_FILE_NAME}

echo "写出软件源配置"

set -x
cat << EOF | sudo tee /etc/apt/sources.list
deb http://mirrors.cloud.aliyuncs.com/debian/ bullseye main non-free contrib
deb http://mirrors.cloud.aliyuncs.com/debian-security/ bullseye-security main
deb http://mirrors.cloud.aliyuncs.com/debian/ bullseye-updates main non-free contrib
deb http://mirrors.cloud.aliyuncs.com/debian/ bullseye-backports main non-free contrib
EOF
set +x

echo -e "\n\n更新源"
sudo apt update

############################################# 时区设置 ##################################################################
echo "############################################# 时区设置 ############################################################"
# 检查timedatectl是否已安装
if ! command -v timedatectl &> /dev/null
then
    echo "timedatectl 未安装，开始下载安装..."
    sudo apt update
    sudo apt install systemd -y
else
    echo "timedatectl 已安装"
fi

# 设置系统时区
echo "设置系统时区为: Asia/Shanghai"
timedatectl set-timezone Asia/Shanghai

############################################# CWD OSC 1337协议 ##########################################################
echo "############################################# CWD OSC 1337协议 ###################################################"
# 检查当前shell是否是bash shell
if [ -z "$BASH" ]
then
    echo "警告：当前shell不是bash shell，无法继续执行脚本"
    exit 1
fi

# 判断是否存在~/.bash_profile文件
if [ ! -f ~/.bash_profile ]
then
    echo "~/.bash_profile 文件不存在，自动创建文件"
    touch ~/.bash_profile
fi


# CWD OSC 1337协议
regex="(PS1)(.*)(PS1)(.*)(1337;CurrentDir)(.*)"
if ! grep -qE "$regex" ~/.bash_profile
then
    echo "写入CWD OSC 1337协议"
    # 在这里执行需要执行的命令
    cat << 'EOF' >> ~/.bash_profile
export PS1="$PS1\[\e]1337;CurrentDir="'$(pwd)\a\]'
EOF
fi

############################################# 常用软件 ##################################################################
echo "############################################# 常用软件 ###########################################################"
apt install -y tcpdump curl wget jq

############################################# 时间同步 ##################################################################
echo "############################################# 时间同步 ############################################################"
# 检查systemd-timesyncd是否已安装
if ! systemctl status systemd-timesyncd &> /dev/null
then
    echo "systemd-timesyncd 未安装，开始下载安装..."
    sudo apt update
    sudo apt install systemd-timesyncd -y
else
    echo "systemd-timesyncd 已安装"
fi


# 修改配置文件
echo -e "\n\n修改systemd-timesyncd 配置文件"
set -x
cat << EOF > /etc/systemd/timesyncd.conf
[Time]
NTP=ntp.cloud.aliyuncs.com ntp1.cloud.aliyuncs.com
# 此参数定义了系统时钟与 NTP 服务器之间的最大偏差值。如果时钟偏差超过这个值，systemd-timesyncd 将不会同步时间。默认情况下是 5 秒
RootDistanceMaxSec=5
# 与 NTP 服务器通信的最小间隔时间。单位是秒。默认值是 32 秒。
PollIntervalMinSec=300
# 与 NTP 服务器通信的最大间隔时间。单位是秒。默认值是 2048 秒。
PollIntervalMaxSec=300
EOF
set +x

sleep 1

# 重启服务
systemctl restart systemd-timesyncd
# 检查应用是否成功重启
retries=0
while [ $retries -lt 100 ]; do
    if systemctl is-active systemd-timesyncd.service &> /dev/null; then
        echo "systemd-timesyncd重启成功."
        break
    fi
    sleep 0.1
    retries=$((retries + 1))
done

# 设置NTP同步
timedatectl set-ntp true

# 验证配置
echo -e "\n\n查看systemd-timesyncd 显示时间同步配置"
timedatectl show-timesync --all

echo -e "\n\n查看systemd-timesyncd 同步状态"
timedatectl timesync-status

# 开机自启动与自启动验证
echo -e "\n\n设置systemd-timesyncd 自启动"
sudo systemctl enable systemd-timesyncd.service
echo -e "systemd-timesyncd 自启动状态"
sudo systemctl is-enabled systemd-timesyncd.service

# 查看运行日志
echo -e "\n\n查看systemd-timesyncd 运行日志"
journalctl -u systemd-timesyncd -n 100 --no-pager

sleep 3

############################################# 关闭swap ##################################################################
echo "############################################# 关闭swap ###########################################################"
# 临时关闭交换分区
echo "临时关闭交换分区"
set -x
sudo swapoff -a
set +x

# 永久关闭交换分区
if [ -f /etc/fstab ]
then
    echo "永久关闭交换分区"
    set -x
    sudo sed -ri 's/.*swap.*/#&/' /etc/fstab
    set +x
fi

# 执行free命令，注意，并闭时total和free处均为0，由此可知swap已经关闭
free
sleep 3


############################################# 流量转发 ##################################################################
echo "############################################# 流量转发 ###########################################################"
echo "临时加载模块"
set -x
sudo modprobe overlay
sudo modprobe br_netfilter
set +x

# 临时应用配置
echo "配置启动时加载模块"
set -x
cat << EOF > /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
set +x

echo "配置内核参数"
set -x
cat << EOF | sudo tee /etc/sysctl.d/99-k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1

# 优化内核参数
# 禁止使用 swap 空间，只有当系统 OOM 时才允许使用它
vm.swappiness=0
# 不检查物理内存是否够用
vm.overcommit_memory=1
# 开启 OOM
vm.panic_on_oom=0
# 增加系统文件描述符的限制。
fs.file-max=52706963
fs.nr_open=52706963
# 禁用IPv6，一些网络环境可能不需要或不支持IPv6。
net.ipv6.conf.all.disable_ipv6=1
EOF
set +x

echo -e "\n\n应用内核参数"
sudo sysctl --system

echo -e "\n\n查看配置情况"
set -x
lsmod | grep br_netfilter
lsmod | grep overlay
sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
set +x

############################################# IPVS #####################################################################
echo "############################################# IPVS ##############################################################"
echo "安装ipset和ipvsadm"
# 安装ipset 和 ipvsadm
apt install -y ipset ipvsadm

echo -e "\n\n临时加载模块"
set -x
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack
set +x

echo -e "\n\n查看模块是否加载成功"
# 查看是否加载成功
lsmod|grep -e ip_vs -e nf_conntrack

# 开机加载模块
echo -e "\n\n配置启动时加载模块"
cat << EOF > /etc/modules-load.d/ipvs.conf
ip_vs
ip_vs_rr
ip_vs_wrr
ip_vs_sh
nf_conntrack
EOF