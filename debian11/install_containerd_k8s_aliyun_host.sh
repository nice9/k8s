#!/bin/bash
# 远程使用方式
# curl -sSL "https://gitee.com/nice9/k8s/raw/master/debian11/install_containerd_k8s_aliyun_host.sh" | bash

############################################# containerd ###############################################################
echo "############################################# containerd ########################################################"
# step 1: 安装必要的一些系统工具
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common

# step 2: 安装GPG证书, 阿里云服务器使用mirrors.cloud.aliyuncs.com域名的
curl -fsSL http://mirrors.cloud.aliyuncs.com/docker-ce/linux/debian/gpg | sudo apt-key add -
# 使用阿里云域名的
# curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/debian/gpg | sudo apt-key add -

# Step 3: 写入软件源信息, 使用mirrors.cloud.aliyuncs.com域名的
sudo add-apt-repository "deb [arch=amd64] http://mirrors.cloud.aliyuncs.com/docker-ce/linux/debian $(lsb_release -cs) stable"
# 使用阿里云域名的
# sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/debian $(lsb_release -cs) stable"

# Step 4: 更新并安装containerd
sudo apt-get -y update
sudo apt-get -y install containerd.io=1.6.28-1

echo -e "\n\n配置containerd自启动"
systemctl enable containerd
sudo systemctl is-enabled containerd

############################################# 配置containerd SystemdCgroup###############################################
echo "############################################# 配置containerd SystemdCgroup #######################################"
echo "生成文件夹: /etc/containerd"
sudo mkdir -p /etc/containerd

echo "生成配置: /etc/containerd/config.toml"
containerd config default > /etc/containerd/config.toml

echo "生成临时配置: /etc/containerd/config.toml.temp"
# 生成临时文件, 临时文件带行号
sudo cat -n  /etc/containerd/config.toml > /etc/containerd/config.toml.temp

# 获取[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]的行号
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$(sudo grep '\[plugins\."io\.containerd\.grpc\.v1\.cri"\.containerd\.runtimes\.runc\.options\]' /etc/containerd/config.toml.temp|awk '{print $1}')
# 判断行号是否存在或小于0
if [ -z "$CONTAINERD_RUNC_OPTIONS_LINE_NUMBER" ] || [ "$CONTAINERD_RUNC_OPTIONS_LINE_NUMBER" -lt 0 ]; then
    echo '[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]配置不存在'
    exit 1
fi
# CONTAINERD_RUNC_OPTIONS_LINE_NUMBER - 1
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_LINE_NUMBER - 1))
# 删除1-CONTAINERD_RUNC_OPTIONS_LINE_NUMBER行
sudo sed -i "1,${CONTAINERD_RUNC_OPTIONS_LINE_NUMBER}d" /etc/containerd/config.toml.temp

# 获取 SystemdCgroup = false的行号
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$(sudo grep -n 'SystemdCgroup = false' /etc/containerd/config.toml.temp|cut -d: -f1)
# 判断行号是否存在或小于0
if [ -z "$CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER" ] || [ "$CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER" -lt 0 ]; then
    echo 'SystemdCgroup 配置不存在'
    exit 1
fi
# CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER + 1
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER + 1))
# 删除CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER到最后的数据
sudo sed -i "${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER},\$d" /etc/containerd/config.toml.temp

echo -e "查找到需要修改的配置片段"
cat /etc/containerd/config.toml.temp

# 更正文件中的行号
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_LINE_NUMBER + 1))
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$(sudo grep 'SystemdCgroup = false' /etc/containerd/config.toml.temp|awk '{print $1}')

# 修改指定行中的数据
sed -i "${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER}s/SystemdCgroup = false/SystemdCgroup = true/" /etc/containerd/config.toml

echo  "修改后配置片段"
cat -n  /etc/containerd/config.toml | sed -n "${CONTAINERD_RUNC_OPTIONS_LINE_NUMBER},${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER}p"

echo "删除生成临时配置: /etc/containerd/config.toml.temp"
rm -rf /etc/containerd/config.toml.temp

echo "重启containerd"
systemctl daemon-reload
systemctl restart containerd.service

echo "查看containerd启动状态"
ps aux | grep containerd | grep -v grep
systemctl status containerd.service -l --no-pager

############################################# 配置containerd sandbox_image###############################################
echo "############################################# 配置containerd sandbox_image #######################################"
echo "生成临时配置: /etc/containerd/config.toml.temp"
# 生成临时文件, 临时文件带行号
sudo cat -n  /etc/containerd/config.toml > /etc/containerd/config.toml.temp

# 获取[plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]的行号
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$(sudo grep '\[plugins\."io\.containerd\.grpc\.v1\.cri"\]' /etc/containerd/config.toml.temp|awk '{print $1}')
# 判断行号是否存在或小于0
if [ -z "$CONTAINERD_RUNC_OPTIONS_LINE_NUMBER" ] || [ "$CONTAINERD_RUNC_OPTIONS_LINE_NUMBER" -lt 0 ]; then
    echo '[plugins."io.containerd.grpc.v1.cri"]配置不存在'
    exit 1
fi
# CONTAINERD_RUNC_OPTIONS_LINE_NUMBER - 1
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_LINE_NUMBER - 1))
# 删除1-CONTAINERD_RUNC_OPTIONS_LINE_NUMBER行
sudo sed -i "1,${CONTAINERD_RUNC_OPTIONS_LINE_NUMBER}d" /etc/containerd/config.toml.temp

# 获取 SystemdCgroup = false的行号
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$(sudo grep -n 'sandbox_image = "registry.k8s.io/pause:3.6"' /etc/containerd/config.toml.temp|cut -d: -f1)
# 判断行号是否存在或小于0
if [ -z "$CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER" ] || [ "$CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER" -lt 0 ]; then
    echo 'sandbox_image = "registry.k8s.io/pause:3.6" 配置不存在'
    exit 1
fi
# CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER + 1
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER + 1))
# 删除CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER到最后的数据
sudo sed -i "${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER},\$d" /etc/containerd/config.toml.temp

echo -e "查找到需要修改的配置片段"
cat /etc/containerd/config.toml.temp

# 更正文件中的行号
CONTAINERD_RUNC_OPTIONS_LINE_NUMBER=$((CONTAINERD_RUNC_OPTIONS_LINE_NUMBER + 1))
CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER=$(sudo grep 'sandbox_image = "registry.k8s.io/pause:3.6"' /etc/containerd/config.toml.temp|awk '{print $1}')

# 修改指定行中的数据
sed -i "${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER}s/sandbox_image = \"registry.k8s.io\/pause:3.6\"/sandbox_image = \"registry.aliyuncs.com\/google_containers\/pause:3.9\"/" /etc/containerd/config.toml

echo  "修改后配置片段"
cat -n  /etc/containerd/config.toml | sed -n "${CONTAINERD_RUNC_OPTIONS_LINE_NUMBER},${CONTAINERD_RUNC_OPTIONS_SYSTEMDCGROUP_LINE_NUMBER}p"

echo "删除生成临时配置: /etc/containerd/config.toml.temp"
rm -rf /etc/containerd/config.toml.temp

echo "重启containerd"
systemctl daemon-reload
systemctl restart containerd.service

echo "查看containerd启动状态"
ps aux | grep containerd | grep -v grep
systemctl status containerd.service -l --no-pager

############################################# Kubernetes ###############################################################
echo "############################################# Kubernetes ########################################################"
##### 增加k8s软件源, 需要注意1.28及以后得版本, k8s软件源的安装方式变了, 参考国内源 阿里云的讲述

# step 1: 安装必要的一些系统工具
apt-get update && apt-get install -y apt-transport-https

# step 2: 安装GPG证书
mkdir -p /etc/apt/keyrings/
curl -fsSL http://mirrors.cloud.aliyuncs.com/kubernetes-new/core/stable/v1.29/deb/Release.key |
    gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

# Step 3: 写入软件源信息
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] http://mirrors.cloud.aliyuncs.com/kubernetes-new/core/stable/v1.29/deb/ /" |
    tee /etc/apt/sources.list.d/kubernetes.list

# Step 4: 更新并安装kubelet kubeadm kubectl
apt-get update
apt-get install -y kubelet=1.29.2-1.1 kubeadm=1.29.2-1.1 kubectl=1.29.2-1.1

# Setp 5: kubelet开机自启动与自启动验证
sudo systemctl enable kubelet
sudo systemctl is-enabled kubelet

systemctl status kubelet.service -l --no-pager

###### 安装指定版本的kubelet:
# Step 1: 查找kubelet的版本:
# apt-cache madison kubelet

# Step 2: 安装指定版本的kubelet:
# sudo apt-get -y install kubelet=[VERSION]

############################################# Bash Completion ##########################################################
echo "############################################# Bash Completion ###################################################"
# 安装bash补全
apt install -y bash-completion

# 执行bash_completion
source /usr/share/bash-completion/bash_completion

# 加载 completion
source <(kubectl completion bash)

# 写入系统配置中
cat << EOF >> ~/.bashrc
source /usr/share/bash-completion/bash_completion
source <(kubectl completion bash)
EOF

############################################# crictl配置 ################################################################
echo "############################################# crictl配置 #########################################################"
set -x
crictl config runtime-endpoint unix:///run/containerd/containerd.sock
crictl config image-endpoint unix:///run/containerd/containerd.sock
crictl config timeout 10
set +x


